import styled from "styled-components";
import PropTypes from "prop-types";

const StyledButton = styled.button`
  background: fff;
  border-radius: 8px;
  border: 1px solid;
  color: black;
  padding: 15px;
  font-size: 16px;

  &:hover {
    background: darkviolet;
    cursor: pointer;
  }
`;

function Button({ children, type = "button", onClick }) {
  return (
    <StyledButton type={type} onClick={onClick}>
      {children}
    </StyledButton>
  );
}
Button.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
