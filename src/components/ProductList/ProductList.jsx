import ProductCard from "../ProductCard/ProductCar";
import styled from "styled-components";
import PropTypes from "prop-types";

const ProductListContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

function ProductList({
  products,
  handleAddToCart,
  handleToggleFavorite,
  favorites,
}) {
  return (
    <ProductListContainer>
      {products.map((product, index) => (
        <ProductCard
          handleAddToCart={handleAddToCart}
          key={index}
          product={product}
          handleToggleFavorite={handleToggleFavorite}
          isFavorite={favorites[product.SKU]}
        />
      ))}
    </ProductListContainer>
  );
}
ProductList.propTypes = {
  products: PropTypes.shape({
    ImageUrl: PropTypes.string.isRequired,
    Name: PropTypes.string.isRequired,
    Price: PropTypes.number.isRequired,
    Color: PropTypes.string.isRequired,
    SKU: PropTypes.string.isRequired,
  }).isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  handleToggleFavorite: PropTypes.func.isRequired,
  favorites: PropTypes.object.isRequired,
};

export default ProductList;
