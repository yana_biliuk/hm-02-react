import Modal from "../Modal";
import ModalClose from "../ModalClose/ModalClose";
import ModalHeader from "../ModalHeader/ModalHeader ";
import ModalBody from "../ModalBody/ModalBody";
import ModalFooter from "../ModalFooter/ModalFooter";
import PropTypes from "prop-types";

function ModalText({ onClose, product }) {
  return (
    <Modal onClose={onClose}>
      <ModalClose onClick={onClose} />
      <ModalHeader> {product?.Name} </ModalHeader>
      <ModalBody>
        <p>Description for you product</p>
      </ModalBody>
      <ModalFooter firstText="ADD TO FAVORITE" firstClick={onClose} />
    </Modal>
  );
}
ModalText.propTypes = {
  onClose: PropTypes.func,
  product: PropTypes.shape({
    Name: PropTypes.string.isRequired,
    Price: PropTypes.number.isRequired,
    ImageUrl: PropTypes.string.isRequired,
    SKU: PropTypes.number.isRequired,
    Color: PropTypes.string,
  }),
};

export default ModalText;
