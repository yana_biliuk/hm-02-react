import styled from "styled-components";
import PropTypes from "prop-types";

const BodyConteiner = styled.div`
  justify-content: center;
  color: black;
  text-align: center;
  font-size: 16px;
  font-weight: 500;
  width: 448px;
  margin-top: 32px;
`;

function ModalBody({ children }) {
  return <BodyConteiner>{children}</BodyConteiner>;
}

ModalBody.propTypes = {
  children: PropTypes.node.isRequired,
};
export default ModalBody;
