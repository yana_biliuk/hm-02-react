import Modal from "../Modal";
import ModalClose from "../ModalClose/ModalClose";
import ModalHeader from "../ModalHeader/ModalHeader ";
import ModalBody from "../ModalBody/ModalBody";
import ModalFooter from "../ModalFooter/ModalFooter";
import CardImage from "../../../img/product.png";

import PropTypes from "prop-types";

function ModalImage({ onClose }) {
  return (
    <Modal onClose={onClose}>
      <ModalClose onClick={onClose} />
      <ModalHeader>
        <img src={CardImage} style={{ height: "200px" }} alt="product" />
      </ModalHeader>
      <ModalBody>
        <h2>PRODUCT DELETE</h2>
        <p>
          By clicling the 'Yes, Delete' button, PRODUCT NAME will be deleted
        </p>
      </ModalBody>
      <ModalFooter
        firstText="NO, CANCEL"
        secondaryText="YES, DELETE"
        firstClick={onClose}
        secondaryClick={onClose}
      />
    </Modal>
  );
}

ModalImage.propTypes = {
  onClose: PropTypes.func,
};

export default ModalImage;
