import styled from 'styled-components';
import Button from '../Button/Button';
import { FaStar } from 'react-icons/fa';
import PropTypes from 'prop-types';




const Card = styled.div`
border: 1px solid #ddd;
border-radius: 8px;
padding: 16px;
margin: 16px;
box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
text-align: center;
background-color: #fff;
display: inline-block;
position: relative;
`;

const ProductImage = styled.img`

width: 150px;
height: 150px;
object-fit: cover;
border-radius: 8px;
margin-bottom: 16px;
`;

const ProductName = styled.h2`
    font-size: 1.5em;
    margin-bottom: 8px;
`;

const ProductDetails = styled.p`
    font-size: 1em;
    margin: 5px 0;
   `

const ButtonContainer = styled.div`
    margin-top: 20px;
`;
const StarIcon = styled(FaStar)`
    position: absolute;
    top: 16px;
    right: 16px;
    cursor: pointer;
    color: ${props => (props.isFavorite ? 'darkviolet' : '#ccc')};
`;


function ProductCard  ({ product, handleAddToCart, handleToggleFavorite, isFavorite })  {
    return (
      
 <Card >
     <StarIcon onClick={() => handleToggleFavorite(product.SKU)} isFavorite={isFavorite}/>
            <ProductImage src={product.ImageUrl} alt={product.Name} style={{ width: '100px' }} />
            <ProductName>{product.Name}</ProductName>
            <ProductDetails>Ціна: {product.Price}грн</ProductDetails>
            <ProductDetails>Колір: {product.Color}</ProductDetails>
            <ProductDetails>Артикул: {product.SKU}</ProductDetails>
            <ButtonContainer>
            <Button onClick={() => handleAddToCart(product)}>Add to Card</Button>
            </ButtonContainer>
           
           
          
        </Card>
      
       
    );
}

ProductCard.propTypes = {
    product: PropTypes.shape({
        ImageUrl: PropTypes.string.isRequired,
        Name: PropTypes.string.isRequired,
        Price: PropTypes.number.isRequired,
        Color: PropTypes.string.isRequired,
        SKU: PropTypes.string.isRequired,
    }).isRequired,
    handleAddToCart: PropTypes.func.isRequired,
    handleToggleFavorite: PropTypes.func.isRequired,
   
};


export default ProductCard;

