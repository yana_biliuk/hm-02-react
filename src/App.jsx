import { useState, useEffect } from "react";
// import Button from "./components/Button/Button";
import ModalText from "./components/Modal/ModalText/ModalText";
// import ModalImage from "./components/Modal/ModalImage/ModalImage";
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";
import styled from "styled-components";
const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 20px;
`;

const Title = styled.h1`
  text-align: center;
  margin-bottom: 40px;
`;

function App() {
  // const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  // const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const [products, setProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState({});

  useEffect(() => {
    const storedFavorites = localStorage.getItem("favorites");
    const storedCart = localStorage.getItem("cart");

    if (storedFavorites) {
      setFavorites(JSON.parse(storedFavorites));
    }

    if (storedCart) {
      setCart(JSON.parse(storedCart));
    }
  }, []);

  useEffect(() => {
    fetch("/data.json")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => setProducts(data))
      .catch((error) => console.error("Error fetching products:", error));
  }, []);

  const handleAddToCart = (product) => {
    setSelectedProduct(product);

    setIsModalOpen(true);
    const updatedCart = [...cart, product];
    setCart(updatedCart);
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  };

  const handleToggleFavorite = (productSKU) => {
    const updatedFavorites = {
      ...favorites,

      [productSKU]: !favorites[productSKU],
    };
    setFavorites(updatedFavorites);
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
    setSelectedProduct(null);
  };

  const cartCount = cart.length;
  const favoritesCount = Object.values(favorites).filter(
    (favorite) => favorite
  ).length;

  return (
    <>
      <Header
        isFavoriteCart={cartCount > 0}
        isFavoriteFavorites={favoritesCount > 0}
        cartCount={cartCount}
        favoritesCount={favoritesCount}
      />
      <Container>
        {/* <Button onClick={() => setIsFirstModalOpen(true)}>
        Open first modal
      </Button>
      <Button onClick={() => setIsSecondModalOpen(true)}>
        Open second modal
      </Button>

      {isFirstModalOpen && (
        <ModalImage onClose={() => setIsFirstModalOpen(false)} />
      )}
      {isSecondModalOpen && (
        <ModalText onClose={() => setIsSecondModalOpen(false)} />
      )} */}

        <Title>Products</Title>
        <ProductList
          handleAddToCart={handleAddToCart}
          products={products}
          handleToggleFavorite={handleToggleFavorite}
          favorites={favorites}
        />
        {isModalOpen && (
          <ModalText onClose={handleCloseModal} product={selectedProduct} />
        )}
      </Container>
    </>
  );
}

export default App;
